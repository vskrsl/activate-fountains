const { performance } = require("perf_hooks");
const { grounds, findResArray } = require("./helpers");

/**
 *
 * @param {*} a
 */
function activateFountains(a) {
	const inputArray = a.map(e => Number(e));
	const inputLength = a.length;
	const { filledArray } = grounds(inputArray);

	let resArray = [];

	for (let j = 0; j < inputLength; j++) {
		let startIndex = j;
		findResArray(filledArray, inputLength, startIndex, resArray);
		j = resArray.length - 1;
	}

	const res = new Set(resArray).size;

	return res;
}

module.exports = activateFountains;
