const { performance } = require("perf_hooks");

let nullsArray = [];

/**
 *
 * @param {array} a
 * @param {number} i
 * @param {number} n
 */
function range(a, i, n) {
	let from = Math.max(i + 1 - a[i], 1);
	let to = Math.min(i + 1 + a[i], n);

	for (let index = from - 1; index < to; index++) {
		nullsArray[i][index] = i;
	}
}

/**
 *
 * @param {*} input
 */
function grounds(input) {
	nullsArray = Array(input.length)
		.fill(null)
		.map(() => Array(input.length).fill(null));

	const n = input.length;

	input.forEach((element, i, input) => {
		range(input, i, n);
	});

	return { filledArray: nullsArray };
}

/**
 *
 * @param {*} array
 * @param {*} inputN
 * @param {*} startIndex
 * @param {*} resArray
 */
function findResArray(array, inputN, startIndex, resArray) {
	let obj = {};
	let endIndex = 0;
	let endLenght = Math.min(inputN, 100);

	for (let index = startIndex; index < Math.min(startIndex + endLenght, inputN); index++) {
		const element = array[index].slice(startIndex);

		if (element.indexOf(null) !== 0) {
			let length = element.indexOf(null) - 1;

			if (length > -1) {
				obj[index] = length;
				endIndex = element.indexOf(null);
			} else {
				obj[index] = element.length;
				endIndex = array[index].length;
			}
		}
	}

	let longestEscape = Object.keys(obj).sort((a, b) => obj[b] - obj[a])[0];

	array[longestEscape]
		.slice(startIndex)
		.slice(0, endIndex)
		.forEach(e => {
			resArray.push(e);
		});

	return resArray;
}

module.exports = {
	grounds: grounds,
	findResArray: findResArray,
};
