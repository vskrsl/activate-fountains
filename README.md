# Activate Fountains v0.2

One of the problems from BOLT Hiring Event in 2019

There is one-dimensional garden of length _n_. In each position of the _n_ length garden, a fountain has been installed. The fountain at the i<sup>th</sup>
position has a value a[i] (where $`1 \le i \le n`$ ) that describes the coverage limit of fountain _i_. A fountain can cover the range from the position
**max( (i -a[i]),1 ) to min( (i+a[i]),n )**.

For example, if garden length _n_ _= 3_ and _a_ _= {1,2,1}_, then:

    For position 1: a[1] = 1, range = 1 to 2
    For position 2: a[2] = 2, range = 1 to 3
    For position 3: a[3] = 1, range = 2 to 3

![alt text](img/bolt_pic_1.png)

In the beginning, all the fountains are switched off. Determine the minimum number of fountains you need to activate so that whole _n_ length garden will be covered by water.
In the example, the 1 fountain at position _a[2]_ covers the whole garden.

**Function description:**

The function must retrun an ineteger that denotes the minimum number of fountains that must be activated to cover the entire garden by water.

fontainActivation has the following parameter:

_a[a[0],...a[n-1]]_: an array of integers.

**Constraints:**

- _$`1 \le n \le 10^5`$_
- _\$`0 \le a[i] \le min( n,100)`$ (where  $`1 \le i \le 10^5`\$)_

**Input format for custom testing**

An array of integersa _[a[0],...a[n-1]]_: an array of integers.

---

**Sample case 0**

Input: [1,1,1]
Output: 1

![alt text](img/bolt2.png)

Explanation: here a = {1,1,1}. If the 2<sup>nd</sup> is active the range from position 1 to 3 will be covered. The total number of fountains needed is 1.

**Sample case 1**

Input: [2,0,0,0]
Output: 2

![alt text](img/bolt3.png)

Explanation: here a = {2,0,0,0}. the 1<sup>st</sup> fountain will cover the range from 1 to 3 and the 4<sup>th</sup> fountain will cover only the position 4. So, total number of founatains needed is 2.
